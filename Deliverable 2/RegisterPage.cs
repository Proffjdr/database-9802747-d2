﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Deliverable_2
{
    public partial class RegisterPage : Form
    {
        public RegisterPage()
        {
            InitializeComponent();
        }

        private void RegisterPage_Load(object sender, EventArgs e)
        {
            SQL.selectQuery("SELECT name FROM Type");
            while (SQL.read.Read())
            {
                comboBoxType.Items.Add(SQL.read[0].ToString());
            }
        }

        private void buttonReturn_Click(object sender, EventArgs e)
        {
            this.Hide();
            Login register = new Login();
            register.ShowDialog();
            this.Close();
        }

        private void buttonRegister_Click(object sender, EventArgs e)
        {
            string uname = "", pword = "", confirmpword = "", fname = "", sname = "", email = "", areacode = "", phone = "", type="";
            bool filledFields = checkContent();

            try
            {
                uname = textBoxUserName.Text.Trim();
                pword = textBoxPassword.Text.Trim();
                confirmpword = textBoxConfirmPassword.Text.Trim();
                fname = textBoxFName.Text.Trim();
                sname = textBoxSName.Text.Trim();
                email = textBoxEmail.Text.Trim();
                areacode = textBoxAreaCode.Text.Trim();
                phone = textBoxPhoneNumber.Text.Trim();
                type = comboBoxType.Text.Trim();
            }

            catch
            {
                MessageBox.Show("Please check your entries and try again");
            }

            if (filledFields == false)
            {
                MessageBox.Show("Please Complete all required fields");
                textBoxUserName.Focus();
                return;
            }

            if (pword != confirmpword)
            {
                MessageBox.Show("Please ensure both passwords match");
                textBoxPassword.Focus();
                return;
            }

            SQL.selectQuery($"Select username from Client where username = '{uname}'");
            if (SQL.read.HasRows)
            {
                MessageBox.Show("This username already exists, please try another");
                textBoxUserName.Focus();
                return;
            }

            try
            {
                SQL.executeQuery($"INSERT into Client values('{uname}','{pword}','{fname}','{sname}','{email}','{areacode}','{phone}','{type}')");
                SQL.selectQuery($"Select username from Client where username = '{uname}'");
                if (SQL.read.HasRows)
                {
                    MessageBox.Show($"You have successfully registered as {uname}");
                    this.Hide();
                    Login register = new Login();
                    register.ShowDialog();
                    this.Close();
                    return;
                }
                else
                {
                    MessageBox.Show($"Please ensure your password is at least 6 characters, and your email and phone information is valid.");
                    textBoxPassword.Focus();
                    
                }
            }

            catch(Exception ex)
            {
                MessageBox.Show("Please check your fields and try again");
                return;
            }

        }

        private bool checkContent()
        {
            bool holdsData = true;
            foreach (Control c in this.Controls)
            {
                if (c is TextBox)
                {
                    if ("".Equals((c as TextBox).Text.Trim()))
                    {
                        holdsData = false;
                    }
                }

                if(c is ComboBox)
                {
                    if("".Equals((c as ComboBox).Text.Trim()))
                    {
                        holdsData = false;
                    }
                }
            }
            return holdsData;
        }

        private void comboBoxType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxType.Text == "new")
            {
                labelType.Text = "5 hours, suitable for a new driver";
            }

            if(comboBoxType.Text == "experienced")
            {
                labelType.Text = "2 hours, suitable for experienced drivers";
            }
        }
    }
}
